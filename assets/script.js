function fetchTasks() {
  return new Promise((resolve, reject) => {
    $.ajax({
      method: "GET",
      url: "http://localhost/kanzucode/assignments/AJAX-PHP/controllers/fetch.php",
      success: function (response) {
        let tasks = JSON.parse(response);
        tasks.forEach(function (task) {
          let li = $("<li>")
            .addClass("list-group-item")
            .attr("data-id", task.id);
          let formCheckDiv = $("<div>").addClass("form-check");
          let div = $("<div>").addClass("form-check");
          let inputCheck = $("<input type='checkbox'/>").addClass(
            "form-check-input"
          );
          let label = $("<label>").addClass("check-label").text(task.task);
          let date = $("<div>")
            .text(new Date(task.createdAt).toLocaleString())
            .addClass("date");
          let editIcon = $("<i>").addClass("fas fa-pencil-alt");
          let deleteIcon = $("<i>").addClass("fas fa-trash");

          li.append(
            formCheckDiv,
            div,
            inputCheck,
            label,
            date,
            editIcon,
            deleteIcon
          );
          $("#todoList").append(li);
        });
        resolve();
      },
      error: function (error) {
        reject(error);
      },
    });
  });
}

function addTask(formData) {
  return new Promise((resolve, reject) => {
    $.ajax({
      method: "POST",
      url: "http://localhost/kanzucode/assignments/AJAX-PHP/controllers/process.php",
      data: formData,
      success: function (response) {
        let task = JSON.parse(response);
        let li = $("<li>").addClass("list-group-item").attr("data-id", task.id);
        let formCheckDiv = $("<div>").addClass("form-check");
        let div = $("<div>").addClass("form-check");
        let inputCheck = $("<input type='checkbox'/>").addClass(
          "form-check-input"
        );
        let label = $("<label>").addClass("check-label").text(task.task);
        let date = $("<div>")
          .text(new Date(task.createdAt).toLocaleString())
          .addClass("date");
        let editIcon = $("<i>").addClass("fas fa-pencil-alt");
        let deleteIcon = $("<i>").addClass("fas fa-trash");

        li.append(
          formCheckDiv,
          div,
          inputCheck,
          label,
          date,
          editIcon,
          deleteIcon
        );
        $("#todoList").prepend(li);
        $("#todoForm")[0].reset();
        resolve();
      },
      error: function (error) {
        reject(error);
      },
    });
  });
}

$(document).ready(() => {
  fetchTasks()
    .then(() => {
      $("#todoForm").submit(function (e) {
        e.preventDefault();
        addTask($("#todoForm").serialize());
      });
    })
    .catch((error) => {
      console.log(error);
    });
});
