<?php
require_once("../database/database.php");

$sql = "SELECT * FROM tasks ORDER BY id DESC";
$result = $conn->query($sql);
$rows = array();
if ($result->num_rows > 0) {
  while ($row = $result->fetch_assoc()) {
    $rows[] = $row;
  }
}
$conn->close();

echo json_encode($rows);
?>
