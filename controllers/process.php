<?php
require_once("../database/database.php");

if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['todoInput'])) {
    $todo_item = $_POST['todoInput'];
    $stmt = $conn->prepare("INSERT INTO tasks (task) VALUES (?)");
    $stmt->bind_param("s", $todo_item);
    $stmt->execute();
    $stmt->close();

    $newTaskId = $conn->insert_id;
    $sql = "SELECT * FROM tasks WHERE id = $newTaskId";
    $result = $conn->query($sql);
    $newTask = $result->fetch_assoc();

    echo json_encode($newTask);
}
?>